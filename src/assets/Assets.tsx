const Images = {
  splashIcon: require('../../assets/splash.png'),
  bannerImg: require('../../assets/banner.png'),
  profileIcon: require('../../assets/profile.jpg'),
  welcomeMain: require('../../assets/main.png'),
  welcomeLogo: require('../../assets/logo.png'),
}
export default Images

