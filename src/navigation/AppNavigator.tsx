import { createStackNavigator } from "react-navigation-stack"

import React from 'react'
import DrawerNavigator from './DrawerNavigator'
import ChangePassword from '@screens/ChangePassword'
import BaseColors from "@theme/Colors"

const navigationOptions = ({ navigation }: any) => ({
  headerStyle: {
    backgroundColor: BaseColors.headerStyleBackgroundColor,
  },
  headerTintColor: BaseColors.headerTintColor,
  headerTitleStyle: {
    color: BaseColors.headerTintColor,
  },
})

const AppNavigator = createStackNavigator({
  DrawerNavigator,
  ChangePassword: {
    screen: ChangePassword,
    navigationOptions
  }
}, {
  initialRouteName: "DrawerNavigator",
  headerMode: "float",
})

export default AppNavigator