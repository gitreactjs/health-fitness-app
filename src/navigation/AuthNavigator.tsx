
import { createStackNavigator } from "react-navigation-stack"

import React from 'react'
import Welcome from '@screens/Welcome'
import SignIn from '@screens/SignIn'
import SignUp from '@screens/SignUp'
import ForgotPassword from "@screens/ForgotPassword"
import ResetPassword from "@screens/ResetPassword"
import OTPVerification from "@screens/OTPVerification"

const AuthNavigator = createStackNavigator({
  welcome: Welcome,
  signin: SignIn,
  signup: SignUp,
  otp: OTPVerification,
  forgot: ForgotPassword,
  reset: ResetPassword
}, {
  initialRouteName: 'welcome',
  headerMode: "none",
  defaultNavigationOptions: {
    cardStyle: { backgroundColor: '#f7f8fc' },
  },
})

export default AuthNavigator