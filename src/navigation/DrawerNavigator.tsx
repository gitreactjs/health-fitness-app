import { createDrawerNavigator } from 'react-navigation-drawer'
import { StyleSheet, View } from 'react-native'

import React from 'react'

import Profile from "@screens/Profile"
import Settings from "@screens/Settings"
import Home from "@screens/Home"
import BaseColors from '@theme/Colors'

import Sidebar from '@components/Sidebar'
import { Header } from '@components/Header'
import { IconWithBadge } from '@components/IconWithBadge'
import { Ionicons } from '@expo/vector-icons'
import ChangePassword from '@screens/ChangePassword'


const DrawerNavigator = createDrawerNavigator(
  {
    Home: {
      screen: Home
    },
    Profile: {
      screen: Profile
    },
    Settings: {
      screen: Settings
    },
    ChangePassword: {
      screen: ChangePassword
    }
  },
  {
    initialRouteName: "Home",
    unmountInactiveRoutes: true,
    contentComponent: props => <Sidebar {...props} />,
    navigationOptions: ({ navigation }: any) => ({
      headerTitle: navigation.state.routes[navigation.state.index].routeName,
      headerStyle: {
        backgroundColor: BaseColors.headerStyleBackgroundColor,
      },
      headerTintColor: BaseColors.headerTintColor,
      headerTitleStyle: {
        color: BaseColors.headerTintColor,
      },
      headerLeft: () => (
        <Header openDrawer={navigation.toggleDrawer} />
      ),
      headerRight: () => (
        <View style={styles.iconContainer}>
          <IconWithBadge name="search" size={24} style={styles.headerLeftIcon} />
          <IconWithBadge name="notifications" size={24} style={styles.headerLeftIcon} count={9} />
        </View>
      )
    })
  }
)


const styles = StyleSheet.create({
  iconContainer: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    width: 100
  },
  headerLeftIcon: {
    marginRight: 20,
    color: BaseColors.white
  }
})

export default DrawerNavigator