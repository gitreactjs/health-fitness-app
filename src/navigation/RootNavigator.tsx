
import React from 'react'
import { createAppContainer } from "react-navigation"


import AppSwitchNavigator from './Navigation'

export const AppContainer = createAppContainer(AppSwitchNavigator)