/**
 * Developed By: Raghulraj
 * React native list of navigations is there
 * 1. Switch Navigation
 * 2. Stack Navigator
 * 3. TopTabNavigator
 * 4. BottomTabNavigator
 * 5. DrawerNavigator
 * 6. CustomNavigator
 */


import React from 'react'

import { createSwitchNavigator } from "react-navigation"
import AppNavigator from './AppNavigator'
import AuthNavigator from './AuthNavigator'

const AppSwitchNavigator = createSwitchNavigator({
  auth: AuthNavigator,
  app: AppNavigator
}, {
  initialRouteName: 'auth'
})

export default AppSwitchNavigator