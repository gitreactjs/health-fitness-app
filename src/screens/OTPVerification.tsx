
import React from "react"
import { StyleSheet, Text, View, Dimensions, Image, Switch, FlatList, TouchableOpacity, Button, TextInput } from 'react-native'
import * as Analytics from 'expo-firebase-analytics'
import { AuthPageHeader } from '@components/AuthPageHeader'
import CodeInput from 'react-native-confirmation-code-input'

export default class OTPVerification extends React.Component<any, any>{

  state = {
    otp: ""
  }

  constructor(props: any) {
    super(props)

  }

  async componentDidMount() {
    console.log("OTPVerification Mount")
    await Analytics.setCurrentScreen('OTPVerificationScreen')
    await Analytics.logEvent('DidMount', {
      name: 'initial',
      screen: 'OTPVerification',
      purpose: 'Opens the internal OTPVerification',
    })
  }

  render() {
    const { navigation } = this.props
    return (
      <View style={styles.container}>
        <AuthPageHeader 
          navigation={navigation}
          navigateTo={"forgot"}
        />
        <Text style={{ color: '#8276f4' }}>Step 1/7</Text>
        <Text style={styles.title}>Verify your number</Text>
        <Text style={{ paddingBottom: 10 }}>We'll text you on 08223780727</Text>
        <CodeInput
          ref="otp"
          keyboardType="numeric"
          codeLength={4}
          activeColor="#555"
          inactiveColor="#ccc"
          className='border-circle'
          compareWithCode='1234'
          autoFocus={false}
          codeInputStyle={{ fontWeight: '800' }}
          onFulfill={() => console.log('Entered')}
          containerStyle={{ marginBottom: 80 }}
        />
        <TouchableOpacity>
          <Text style={{ color: '#8276f4' }}>Send me a new code</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.button}>
          <Text style={styles.buttonText}>Continue</Text>
        </TouchableOpacity>
      </View>)
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f7f8fc',
    alignItems: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    paddingVertical: 18,
    color: '#000'
  },
  button: {
    width: '57%',
    backgroundColor: '#7165e3',
    borderRadius: 12,
    marginTop: 40,
    marginBottom: 20,
    paddingVertical: 15
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: 'bold'
  }
})