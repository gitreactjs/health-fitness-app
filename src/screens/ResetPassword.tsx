
import React from "react"
import { StyleSheet, Text, View, Image, Switch, FlatList, TouchableOpacity, Button, TextInput } from 'react-native'

import * as Analytics from 'expo-firebase-analytics'
import { AuthStyles } from "@theme/BaseStyles"


export default class ResetPassword extends React.Component<any, any>{

    state = {
      password: "",
      cpassword: ""
    }

    constructor(props: any) {
      super(props)

    }

    async componentDidMount() {
      console.log("ResetPassword Mount")
      await Analytics.setCurrentScreen('ResetPasswordScreen')
      await Analytics.logEvent('DidMount', {
        name: 'initial',
        screen: 'ResetPassword',
        purpose: 'Opens the internal ResetPassword',
      })
    }

    render() {
      const { navigation } = this.props
      return (<View style={AuthStyles.container}>
        <Text style={AuthStyles.logo}>BareApp</Text>
        <View style={AuthStyles.inputView} >
          <TextInput
            secureTextEntry
            style={AuthStyles.inputText}
            placeholder="Password..."
            placeholderTextColor="#003f5c"
            onChangeText={text => this.setState({ password: text })} />
        </View>
        <View style={AuthStyles.inputView} >
          <TextInput
            secureTextEntry
            style={AuthStyles.inputText}
            placeholder="Confirm Password..."
            placeholderTextColor="#003f5c"
            onChangeText={text => this.setState({ cpassword: text })} />
        </View>
        <TouchableOpacity  onPress={() => navigation.navigate('forgot')}>
          <Text style={AuthStyles.forgot}>Forgot Password?</Text>
        </TouchableOpacity>
        <TouchableOpacity style={AuthStyles.loginBtn}  onPress={() => navigation.navigate('signin')}>
          <Text style={AuthStyles.loginText}>ResetPassword</Text>
        </TouchableOpacity>
        <TouchableOpacity  onPress={() => navigation.navigate('signin')}>
          <Text style={AuthStyles.loginText}>SignIn</Text>
        </TouchableOpacity>
      </View>)
    }

}


const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    paddingTop: 40,
    alignItems: "center",
    flex: 1
  }
})