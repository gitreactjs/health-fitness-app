
import React from "react"
import { StyleSheet, Text, View, Image, Switch, FlatList, TouchableOpacity, Button } from 'react-native'

import * as Analytics from 'expo-firebase-analytics'


export default class ChangePassword extends React.Component<any, any>{

  constructor(props: any) {
    super(props)

  }

  async componentDidMount() {
    console.log("ChangePassword Mount")
    await Analytics.setCurrentScreen('ChangePassword')
    await Analytics.logEvent('DidMount', {
      name: 'initial',
      screen: 'changepassword',
      purpose: 'Opens the internal ChangePassword',
    })
  }

  render() {
    const { navigation } = this.props
    return (<View style={styles.container}>
      <Text style={{ padding: 20 }}>
                ChangePassword.
      </Text>
      <Button title="SignIn"
        onPress={() => navigation.navigate('signin', {
          itemId: 86,
          otherParam: 'anything you want here',
        })}
      />
      <Text style={{ padding: 20 }}>
                In eleifend, turpis sit amet suscipit tincidunt, felis ex tempor tellus, at commodo nunc massa rhoncus dui. Vestibulum at malesuada elit.
      </Text>
    </View>)
  }

}


const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    paddingTop: 40,
    alignItems: "center",
    flex: 1
  }
})