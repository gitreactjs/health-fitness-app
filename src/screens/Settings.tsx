import React from "react"
import { StyleSheet, Text, View, Image, Switch, FlatList, TouchableOpacity } from 'react-native'
import * as Analytics from 'expo-firebase-analytics'
import Assets from "@assets/Assets"


export default class Settings extends React.Component<any, any>{

  state = {
    title: 'Home'
  }
  constructor(props: any) {
    super(props)

  }

  async componentDidMount() {
    console.log("Settings Mount")
    await Analytics.setCurrentScreen('SettingsScreen')
    await Analytics.logEvent('DidMount', {
      name: 'initial',
      screen: 'settings',
      purpose: 'Opens the internal Settings',
    })
  }

  render() {
    const { navigation } = this.props
    return (<View style={styles.container}>

      <Image source={Assets.bannerImg} style={{ width: "80%", height: "30%" }} resizeMode="contain" />
      <Text style={{ padding: 20 }}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sit amet dictum sapien, nec viverra orci. Morbi sed maximus purus. Phasellus quis justo mi. Nunc ut tellus lectus.
      </Text>
      <Text style={{ padding: 20 }}>
        In eleifend, turpis sit amet suscipit tincidunt, felis ex tempor tellus, at commodo nunc massa rhoncus dui. Vestibulum at malesuada elit.
      </Text>
    </View>)
  }

}


const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    paddingTop: 40,
    alignItems: "center",
    flex: 1
  }
})