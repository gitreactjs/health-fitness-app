
import React from "react"
import { StyleSheet, Text, View, Image, Switch, FlatList, TouchableOpacity, Button, TextInput } from 'react-native'

import * as Analytics from 'expo-firebase-analytics'
import { AuthStyles } from "@theme/BaseStyles"


export default class SignUp extends React.Component<any, any>{

    state = {
      firstname: "",
      lastname: "",
      email: "",
      password: "",
      cpassword: ""
    }

    constructor(props: any) {
      super(props)

    }

    async componentDidMount() {
      console.log("SignUp Mount")
      await Analytics.setCurrentScreen('SignUpScreen')
      await Analytics.logEvent('DidMount', {
        name: 'initial',
        screen: 'signup',
        purpose: 'Opens the internal signup',
      })
    }

    render() {
      const { navigation } = this.props
      return (<View style={AuthStyles.container}>
        <Text style={AuthStyles.logo}>BareApp</Text>
        <View style={AuthStyles.inputView} >
          <TextInput
            style={AuthStyles.inputText}
            placeholder="FirstName..."
            placeholderTextColor="#003f5c"
            onChangeText={text => this.setState({ firstname: text })} />
        </View>
        <View style={AuthStyles.inputView} >
          <TextInput
            style={AuthStyles.inputText}
            placeholder="LastName..."
            placeholderTextColor="#003f5c"
            onChangeText={text => this.setState({ lastname: text })} />
        </View>
        <View style={AuthStyles.inputView} >
          <TextInput
            style={AuthStyles.inputText}
            placeholder="Email..."
            placeholderTextColor="#003f5c"
            onChangeText={text => this.setState({ email: text })} />
        </View>
        <View style={AuthStyles.inputView} >
          <TextInput
            secureTextEntry
            style={AuthStyles.inputText}
            placeholder="Password..."
            placeholderTextColor="#003f5c"
            onChangeText={text => this.setState({ password: text })} />
        </View>
        <View style={AuthStyles.inputView} >
          <TextInput
            secureTextEntry
            style={AuthStyles.inputText}
            placeholder="Confirm Password..."
            placeholderTextColor="#003f5c"
            onChangeText={text => this.setState({ cpassword: text })} />
        </View>
        <TouchableOpacity onPress={() => navigation.navigate('forgot')}>
          <Text style={AuthStyles.forgot} >Forgot Password?</Text>
        </TouchableOpacity>
        <TouchableOpacity style={AuthStyles.loginBtn} onPress={() => navigation.navigate('otp')}>
          <Text style={AuthStyles.loginText} >SignUp</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('signin')}>
          <Text style={AuthStyles.loginText} >SignIn</Text>
        </TouchableOpacity>
      </View>)
    }

}


const styles = StyleSheet.create({
 
})