
import React from "react"
import { StyleSheet, Text, View, Image, Switch, FlatList, TouchableOpacity, Button, TextInput } from 'react-native'

import * as Analytics from 'expo-firebase-analytics'
import { AuthStyles } from "@theme/BaseStyles"


export default class ForgotPassword extends React.Component<any, any>{

    state = {
      email: ""
    }

    constructor(props: any) {
      super(props)

    }

    async componentDidMount() {
      console.log("ForgotPassword Mount")
      await Analytics.setCurrentScreen('ForgotPasswordScreen')
      await Analytics.logEvent('DidMount', {
        name: 'initial',
        screen: 'ForgotPassword',
        purpose: 'Opens the internal ForgotPassword',
      })
    }

    render() {
      const { navigation } = this.props
      return (<View style={AuthStyles.container}>
        <Text style={AuthStyles.logo}>BareApp</Text>
        <View style={AuthStyles.inputView} >
          <TextInput
            style={AuthStyles.inputText}
            placeholder="Email..."
            placeholderTextColor="#003f5c"
            onChangeText={text => this.setState({ email: text })} />
        </View>
            
           
        <TouchableOpacity style={AuthStyles.loginBtn} onPress={() => navigation.navigate('otp')}>
          <Text style={AuthStyles.loginText} >ForgotPassword</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('signin')}>
          <Text style={AuthStyles.loginText} >SignIn</Text>
        </TouchableOpacity>
      </View>)
    }
}

const styles = StyleSheet.create({
    
})