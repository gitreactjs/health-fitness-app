
import React from "react"
import { StyleSheet, Text, View, TouchableOpacity, TextInput } from 'react-native'
import * as Google from 'expo-google-app-auth'

import * as Analytics from 'expo-firebase-analytics'
import { AuthStyles } from "@theme/BaseStyles"

const IOS_CLIENT_ID = '27736778345-ck07elesr2oqhnquta0n80sg0mre0ai1.apps.googleusercontent.com'
const AND_CLIENT_ID = '27736778345-a0pa8u16b4ondqqgrootd8d24eq7mpi3.apps.googleusercontent.com'
const WEB_CLIENT_ID = '27736778345-3831ppuahss242kd1mrd9l4dal15ii5b.apps.googleusercontent.com'

export default class SignIn extends React.Component<any, any>{

    state = {
      email: "",
      password: ""
    }

    constructor(props: any) {
      super(props)

    }

    async componentDidMount() {
      console.log("SigIn Mount")
      await Analytics.setCurrentScreen('SignInScreen')
      await Analytics.logEvent('DidMount', {
        name: 'initial',
        screen: 'signin',
        purpose: 'Opens the internal signin',
      })
    }


    async signInWithGoogleAsync() {
      try {
        console.log("signInGoogle")
        const result = await Google.logInAsync({
        //  behavior: 'web',
          iosClientId: IOS_CLIENT_ID,
          androidClientId: AND_CLIENT_ID,
          androidStandaloneAppClientId: AND_CLIENT_ID,
          //  webClientId: WEB_CLIENT_ID,
          scopes: ['profile', 'email'],
        })
        console.log(result)
  
        if (result.type === 'success') {
          return result.accessToken
        } else {
          return { cancelled: true }
        }
      } catch (e) {
        return { error: true }
      }

    }

    signInWithGoogle = () => {
      this.signInWithGoogleAsync()
    }

    render() {
      const { navigation } = this.props
      return (<View style={AuthStyles.container}>
        <Text style={AuthStyles.logo}>BareApp</Text>
        <View style={AuthStyles.inputView} >
          <TextInput
            style={AuthStyles.inputText}
            placeholder="Email..."
            placeholderTextColor="#003f5c"
            onChangeText={text => this.setState({ email: text })} />
        </View>
        <View style={AuthStyles.inputView} >
          <TextInput
            secureTextEntry
            style={AuthStyles.inputText}
            placeholder="Password..."
            placeholderTextColor="#003f5c"
            onChangeText={text => this.setState({ password: text })} />
        </View>
        <TouchableOpacity onPress={() => navigation.navigate('forgot')}>
          <Text style={AuthStyles.forgot} >Forgot Password?</Text>
        </TouchableOpacity>
        <TouchableOpacity style={AuthStyles.loginBtn} onPress={() => navigation.navigate('app')}>
          <Text style={AuthStyles.loginText} >SignIn</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('signup')}>
          <Text style={AuthStyles.loginText} >Signup</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => this.signInWithGoogle()}>
          <Text>Sign in with Google</Text>
        </TouchableOpacity>
      </View>)
    }
}

const styles = StyleSheet.create({
    
})

