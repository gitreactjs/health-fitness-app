import React from "react"
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native'
import Assets from "@assets/Assets"

export default class Welcome extends React.Component<any, any>{
  render() {  
    const { navigation } = this.props  
    return (
      <View style={styles.container}>
        <Image source={Assets.welcomeLogo} style={styles.logo} resizeMode="contain" />
        <Text
          style={styles.title}>
                    Welcome to {"\n"}
          <Text style={{ color: '#7b6fe2' }}> Momotaro </Text>
                    UI Kit
        </Text>
        <Text
          style={styles.text}
        >
                    The best UI Kit for your next {"\n"} health and fitness project!
        </Text>
        <Image source={Assets.welcomeMain} style={{ width: "100%" }} resizeMode="contain" />
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('app')}>
          <Text style={styles.buttonText}>Get Started</Text>
        </TouchableOpacity>
        <Text>Already have account? <Text style={{ color: '#8276f4' }} onPress={() => navigation.navigate('signin')}>Sign in</Text></Text>
      </View>
    )
  }

}


const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f7f8fc',
    alignItems: 'center',
    flex: 1
  },
  logo: {
    marginTop: 35,
    marginBottom: 20
  },
  title: {
    fontWeight: 'bold',
    fontSize: 25,
    color: '#000',
    textAlign: 'center',
    fontFamily: 'NotoSans-Regular'
  },
  text: {
    marginVertical: 15,
    textAlign: 'center',
    lineHeight: 21,
    fontSize: 13,
    fontWeight: 'bold'
  },
  button: {
    width: '57%',
    backgroundColor: '#7165e3',
    borderRadius: 12,
    marginTop: 40,
    marginBottom: 20,
    paddingVertical: 12
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: 'bold'
  }
})
