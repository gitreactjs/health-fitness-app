

import { StyleSheet } from 'react-native'


export const BaseStyles = StyleSheet.create({
  container: {

  }
})

export const AuthStyles = StyleSheet.create({
  container:{
    backgroundColor: '#fff',
    alignItems: "center",
    justifyContent: 'center',
    flex: 1
  },
  logo: {
    fontWeight: "bold",
    fontSize: 50,
    color: "#fb5b5a",
    marginBottom: 40
  },
  inputView: {
    width: "80%",
    backgroundColor: "#fff",
    borderRadius: 25,
    borderBottomWidth: 1,
    borderBottomColor: "#fb5b5a", 
    height: 30,
    marginBottom: 20,
    justifyContent: "center",
    padding: 5
  },
  inputText: {
    height: 30,
    color: "black",
    padding: 5
         
  },
  forgot: {
    color: "black",
    fontSize: 11
  },
  loginBtn: {
    width: "80%",
    backgroundColor: "#fb5b5a",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 10
  },
  loginText: {
    color: "black"
  }
})

export const AppStyles = StyleSheet.create({

})


export default BaseStyles