

const BaseColors = {
  white: '#ffffff',
  black: '#00000',
  headerStyleBackgroundColor: '#f4511e',
  headerTintColor: '#fff'
}


export default BaseColors