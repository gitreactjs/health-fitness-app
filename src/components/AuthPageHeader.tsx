
import React from "react"
import { StyleSheet, View } from 'react-native'
import * as Progress from 'react-native-progress'
import { Ionicons } from '@expo/vector-icons'


export const AuthPageHeader = (props: any) => (
  <View style={styles.header}>
    <Ionicons
      style={{ alignSelf: 'flex-start' }}
      name="ios-chevron-back-outline"
      size={28}
      color="black"
      onPress={() => props.navigation.navigate(props.navigateTo)}
    />
    <Progress.Bar
      style={styles.bar}
      color={'#7f73e5'}
      unfilledColor={'#e1def4'}
      borderColor={'transparent'}
      progress={0.5}
      width={100}
    />
  </View>
)


const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    width: '100%',
    paddingTop: 15,
    paddingLeft: 2,
    marginBottom: 40
  },
  bar: {
    marginRight: 'auto',
    marginLeft: 'auto',
    alignSelf: 'center'
  }
})