import React from "react"
import { StyleSheet, Text, View, Image, Switch, FlatList, TouchableOpacity } from 'react-native'

import { Ionicons } from '@expo/vector-icons'

import Assets from "@assets/Assets"

interface State {
    email: string;
    name: string;
    routes: any;
}
interface Props {
    navigation: any
}
export default class Sidebar extends React.Component<Props, State> {
    state: State = {
      email: 'raghulraj.nkl@gmail.com',
      name: 'Raghul',
      routes: [
        {
          name: "Home",
          icon: "ios-home"
        },
        {
          name: "Profile",
          icon: "ios-contact"
        },
        {
          name: "Settings",
          icon: "ios-settings"
        },
        {
          name: "ChangePassword",
          icon: "ios-settings"
        }
      ]
    }


    render() {

      return (
        <View style={styles.container}>
          <Image source={Assets.profileIcon} style={styles.profileImg} />
          <Text style={{ fontWeight: "bold", fontSize: 16, marginTop: 10 }}>{this.state.name}</Text>
          <Text style={{ color: "gray", marginBottom: 10 }}>{this.state.email}</Text>
          <View style={styles.sidebarDivider}></View>
          <FlatList
            style={{ width: "100%", marginLeft: 30 }}
            data={this.state.routes}
            renderItem={({ item }) => <Item item={item} navigate={this.props.navigation.navigate} />}
            keyExtractor={item => item.name}
          />
        </View>
      )
    }
}

const Item = ({ item, navigate }: any)=> (
  <TouchableOpacity style={styles.listItem} onPress={() => navigate(item.name)}>
    <Ionicons name={item.icon} size={18} style={styles.icon}/>
    <Text style={styles.title}>{item.name}</Text>
  </TouchableOpacity>
)
const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    paddingTop: 15,
    alignItems: "center",
    flex: 1

  },
  listItem: {
    height: 45,
    alignItems: "center",
    flexDirection: "row",
    
  },
  icon: {
    width: 20,
    paddingLeft: 5
  },
  title: {
    fontSize: 16,
    marginLeft: 20
  },
   
  profileImg: {
    width: 80,
    height: 80,
    borderRadius: 40,
    marginTop: 20
  },
  sidebarDivider: {
    height: 1,
    width: "100%",
    backgroundColor: "lightgray",
    marginVertical: 10
  }
})