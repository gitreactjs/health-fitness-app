import { Ionicons } from "@expo/vector-icons"
import React from "react"
import { StyleSheet, View, Text } from "react-native"




export const IconWithBadge = ({ name, color, size, style, count }: any) => (
  <View style={styles.wrapper}>
    <Ionicons name={name} size={size} color={color} style={style} />
    {count ? <View style={styles.innerContainer}>
      <Text>{count}</Text>
    </View> : <></>}
  </View>
)


const styles = StyleSheet.create({

  wrapper: {
    width: 50,
    height: 50,
    marginTop: 25,
    marginBottom: 0,
    color: "white"
  },
  innerContainer: {
    position: "absolute",
    right: 20,
    top: -5,
    backgroundColor: 'yellow',
    borderRadius: 8,
    width: 16,
    height: 16,
    justifyContent: "center",
    alignItems: "center"
  }
})