
import React from "react"
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'

import { Ionicons } from '@expo/vector-icons'


export const Header = ({ openDrawer }: any) => (
  <View style={styles.header}>
    <TouchableOpacity onPress={() => openDrawer()}>
      <Ionicons name="ios-menu" size={32} color="white"/>
    </TouchableOpacity>  
  </View>
)


const styles = StyleSheet.create({
  header: {
    width: "100%",
    height: 50,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 20
  }
})