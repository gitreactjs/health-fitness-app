import { registerRootComponent } from 'expo';
import {LogBox} from 'react-native';

import App from './App';

// registerRootComponent calls AppRegistry.registerComponent('main', () => App);
// It also ensures that whether you load the app in the Expo client or in a native build,
// the environment is set up appropriately
LogBox.ignoreAllLogs(true);
LogBox.ignoreLogs([
    "Remote debugger is in a background tab which may cause apps to perform slowly. Fix this by foregrounding the tab (or opening it in a separate window)", 
    "Your project is accessing the following APIs from a deprecated global rather than a module import: Constants (expo- constants).",
    'The global "__expo" and "Expo" objects will be removed in SDK 41. Learn more about how to fix this warning: https://expo.fyi/deprecated-globals'
    // name of the error/warning here, or a regex here
  ]);
  
registerRootComponent(App);
