module.exports = function (api) {
  api.cache(true);
  return {
    presets: ['babel-preset-expo', 'module:metro-react-native-babel-preset'],
    plugins: [
      [
        'module-resolver',
        {
          root: ['.'],
          extensions: [
            '.ios.ts',
            '.android.ts',
            '.ts',
            '.ios.tsx',
            '.android.tsx',
            '.tsx',
            '.jsx',
            '.js',
            '.json',
          ],
          alias: {
            '@config': './src/config',
            '@constants': './src/constants',
            '@theme': './src/theme',
            '@layout': './src/layout',
            '@components': './src/components',
            '@screens': './src/screens',
            '@assets': './src/assets',
            '@theme': './src/theme',
            '@utils': './src/utils'
          },
        },
      ],
    ],
  };
};
