import React from 'react';
import Constants from 'expo-constants';
import { AppContainer } from './src/navigation/RootNavigator';
import * as Analytics from 'expo-firebase-analytics';
import * as Location from 'expo-location';
import NetInfo from '@react-native-community/netinfo';
import { SafeAreaView, StyleSheet } from 'react-native';
import FlashMessage from 'react-native-flash-message';

export default class App extends React.Component {

  state = {
    location: ''
  }

  constructor(Props: any) {
    super(Props)
    console.log(Constants.manifest.extra);
     
  }

  async componentDidMount(){
    console.log("App Mount");
    await Analytics.setAnalyticsCollectionEnabled(false);
    let { status } = await Location.requestPermissionsAsync();
    console.log("Location Status", status);
    if (status !== 'granted') {
      return;
    }
    let location = await Location.getCurrentPositionAsync({});
    console.log("Current Location",location);
    this.setState({location})

    NetInfo.fetch().then(state => {
      console.log('Connection type', state.type);
      console.log('Is connected?', state.isConnected);
    });

  }

  getActiveRouteName(navigationState: any):any {
    if (!navigationState) return null;
    const route = navigationState.routes[navigationState.index];
    // Parse the nested navigators
    if (route.routes) return this.getActiveRouteName(route);
    return route.routeName;
  }


  render(){
    return (
      <SafeAreaView style={styles.container}>
      <FlashMessage position="bottom" />
      <AppContainer onNavigationStateChange={(prevState, currentState) => {
        const currentScreen = this.getActiveRouteName(currentState);
        const prevScreen = this.getActiveRouteName(prevState);
        if (prevScreen !== currentScreen) {
          Analytics.setCurrentScreen(currentScreen);
        }        
      }} />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
